import 'package:anime_player/core/constants/routes_path.dart';
import 'package:anime_player/features/home/presentation/views/home_view.dart';
import 'package:anime_player/features/register/presentation/views/login_view.dart';
import 'package:anime_player/features/register/presentation/views/sign_up_view.dart';
import 'package:anime_player/features/splash/presentation/views/splash_view.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

GoRouter routes = GoRouter(routes: [
  GoRoute(
    path: Routes.splashView,
    builder: (BuildContext context, GoRouterState state) => const SplashView(),
  ),
  GoRoute(
    path: Routes.signUpView,
    builder: (BuildContext context, GoRouterState state) => const SignUpView(),
  ),
  GoRoute(
    path: Routes.logInView,
    builder: (BuildContext context, GoRouterState state) => const LoginView(),
  ),
  GoRoute(
    path: Routes.homeView,
    builder: (BuildContext context, GoRouterState state) => const HomeView(),
  ),
]);
