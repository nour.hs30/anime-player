import 'package:flutter/material.dart';

abstract class AppColors {
  static const lightTheme = false;
  static const primaryColor = Color.fromARGB(255, 12, 205, 230);
}
