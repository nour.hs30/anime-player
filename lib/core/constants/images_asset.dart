abstract class ImagesAsset {
  static const bigLogo = "assets/images/logo.svg";
  static const logo = "assets/images/logo2.svg";
  static const openedEye = "assets/images/opned_eye.svg";
  static const closedEye = "assets/images/closed_eye.svg";
  static const filterIcon = "assets/images/filter_icon.svg";
  static const searchIcon = "assets/images/search_icon.svg";

  static const fourSquare = "assets/images/four_square.svg";
  static const sixSquare = "assets/images/six_square.svg";
  static const twoColumns = "assets/images/two_columns.svg";
  static const openDrawer = "assets/images/open_drawer.svgg";
}
