import 'package:flutter/material.dart';

abstract class FontsStyle {
  static const font25Bold = TextStyle(
    fontSize: 25,
    fontWeight: FontWeight.bold,
  );
}
