import 'package:anime_player/core/constants/images_asset.dart';
import 'package:anime_player/core/constants/routes_path.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:go_router/go_router.dart';

class SplashViewBody extends StatefulWidget {
  const SplashViewBody({super.key});

  @override
  State<SplashViewBody> createState() => _SplashViewBodyState();
}

class _SplashViewBodyState extends State<SplashViewBody> {
  @override
  void initState() {
    timeOut();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SvgPicture.asset(
        ImagesAsset.bigLogo,
        width: MediaQuery.sizeOf(context).width * .5,
        height: MediaQuery.sizeOf(context).height * .25,
      ),
    );
  }

  timeOut() {
    Future.delayed(
      const Duration(seconds: 2),
      () => context.go(Routes.signUpView),
    );
  }
}
