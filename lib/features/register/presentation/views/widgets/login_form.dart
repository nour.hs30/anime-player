import 'package:anime_player/core/constants/routes_path.dart';
import 'package:anime_player/features/register/presentation/views/widgets/auth_button.dart';
import 'package:anime_player/features/register/presentation/views/widgets/email_text_field.dart';
import 'package:anime_player/features/register/presentation/views/widgets/password_text_field.dart';
import 'package:anime_player/features/register/presentation/views/widgets/sign_up_widget.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class LoginForm extends StatelessWidget {
  const LoginForm({
    super.key,
    required this.formKey,
  });

  final GlobalKey<FormState> formKey;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          EmailTextField(
            validatorCon: (value) {
              if (value == null || value.isEmpty) {
                return "The Field Is Required";
              }
              return null;
            },
          ),
          const SizedBox(
            height: 16,
          ),
          PasswordTextField(
            validatorCon: (value) {
              if (value == null || value.isEmpty) {
                return "The Field Is Required";
              }
              return null;
            },
          ),
          const SizedBox(
            height: 8,
          ),
          const SignUpWidget(),
          const SizedBox(
            height: 16,
          ),
          AuthButton(
            onPressed: () {
              if (formKey.currentState!.validate()) {
                context.go(Routes.homeView);
              }
            },
          ),
        ],
      ),
    );
  }
}
