import 'package:anime_player/core/constants/images_asset.dart';
import 'package:anime_player/features/register/presentation/views/widgets/login_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class LoginViewBody extends StatefulWidget {
  const LoginViewBody({super.key});

  @override
  State<LoginViewBody> createState() => _LoginViewBodyState();
}

class _LoginViewBodyState extends State<LoginViewBody> {
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.sizeOf(context).height;
    return SafeArea(
      child: Column(
        children: [
          const SizedBox(
            height: 32,
          ),
          SvgPicture.asset(
            ImagesAsset.bigLogo,
            height: height * .25,
          ),
          const SizedBox(
            height: 18,
          ),
          const Text(
            "Log In",
            style: TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.w700,
            ),
          ),
          const SizedBox(
            height: 64,
          ),
          Expanded(
            child: LoginForm(formKey: formKey),
          ),
        ],
      ),
    );
  }
}
