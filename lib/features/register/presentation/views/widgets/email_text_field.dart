import 'package:flutter/material.dart';

class EmailTextField extends StatelessWidget {
  const EmailTextField({super.key, this.validatorCon});
  final String? Function(String?)? validatorCon;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: TextFormField(
        validator: validatorCon,
        decoration: InputDecoration(
          border: inputBorderFun(choosedColor: Colors.black),
          focusedBorder: inputBorderFun(choosedColor: Colors.black),
          enabledBorder: inputBorderFun(choosedColor: Colors.black),
          errorBorder: inputBorderFun(choosedColor: Colors.red),
        ),
      ),
    );
  }

  inputBorderFun({required Color choosedColor}) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(16),
      borderSide: BorderSide(color: choosedColor),
    );
  }
}
