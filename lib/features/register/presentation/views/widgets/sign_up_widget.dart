import 'package:anime_player/core/constants/colors.dart';
import 'package:anime_player/core/constants/routes_path.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SignUpWidget extends StatelessWidget {
  const SignUpWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SizedBox(
          width: 20,
        ),
        GestureDetector(
          onTap: () {
            context.go(Routes.signUpView);
          },
          child: const Text(
            "Already Has An Account",
            style: TextStyle(
              color: AppColors.primaryColor,
            ),
            textAlign: TextAlign.start,
          ),
        ),
      ],
    );
  }
}
