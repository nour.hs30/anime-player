import 'package:anime_player/core/constants/font_style.dart';
import 'package:anime_player/core/constants/images_asset.dart';

import 'package:anime_player/features/register/presentation/views/widgets/sign_up_form.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class SignUpViewBody extends StatefulWidget {
  const SignUpViewBody({super.key});

  @override
  State<SignUpViewBody> createState() => _SignUpViewBodyState();
}

class _SignUpViewBodyState extends State<SignUpViewBody> {
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.sizeOf(context).height;
    return SafeArea(
      child: Column(
        children: [
          const SizedBox(
            height: 32,
          ),
          SvgPicture.asset(
            ImagesAsset.bigLogo,
            height: height * .25,
          ),
          const SizedBox(
            height: 18,
          ),
          const Text(
            "Create An Account",
            style: FontsStyle.font25Bold,
          ),
          const SizedBox(
            height: 64,
          ),
          Expanded(
            child: SginUpForm(formKey: formKey),
          ),
        ],
      ),
    );
  }
}
