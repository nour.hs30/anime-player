import 'package:anime_player/core/constants/colors.dart';
import 'package:anime_player/core/constants/images_asset.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class PasswordTextField extends StatefulWidget {
  const PasswordTextField({
    super.key,
    this.validatorCon,
  });
  final String? Function(String?)? validatorCon;
  @override
  State<PasswordTextField> createState() => _PasswordTextFieldState();
}

class _PasswordTextFieldState extends State<PasswordTextField> {
  bool obscureTextVar = true;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: TextFormField(
        validator: widget.validatorCon,
        obscureText: obscureTextVar,
        decoration: InputDecoration(
          border: inputBorderFun(choosedColor: Colors.black),
          focusedBorder: inputBorderFun(choosedColor: Colors.black),
          enabledBorder: inputBorderFun(choosedColor: Colors.black),
          errorBorder: inputBorderFun(choosedColor: Colors.red),
          suffixIcon: GestureDetector(
            onTap: () {
              obscureTextVar = !obscureTextVar;
              setState(() {});
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 8,
              ),
              child: SvgPicture.asset(
                color: AppColors.lightTheme ? Colors.black : Colors.white,
                obscureTextVar ? ImagesAsset.closedEye : ImagesAsset.openedEye,
              ),
            ),
          ),
        ),
      ),
    );
  }

  inputBorderFun({required Color choosedColor}) {
    return OutlineInputBorder(
      borderRadius: BorderRadius.circular(16),
      borderSide: BorderSide(color: choosedColor),
    );
  }
}
