class VideoEntity {
  final String? imageUri;
  final String? title;
  final double? rating;
  final num? episodeNumber;
  final String? status;

  VideoEntity({
    required this.imageUri,
    required this.title,
    required this.rating,
    required this.episodeNumber,
    required this.status,
  });
  List<VideoEntity> homeList = [];
}
