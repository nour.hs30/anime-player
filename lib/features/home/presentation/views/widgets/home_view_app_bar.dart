import 'package:anime_player/core/constants/colors.dart';
import 'package:anime_player/core/constants/images_asset.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class HomeViewAppBar extends StatelessWidget {
  const HomeViewAppBar({super.key});

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 7,
      child: Container(
        color: AppColors.primaryColor,
        child: Row(
          children: [
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset(ImagesAsset.filterIcon),
            ),
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset(ImagesAsset.searchIcon),
            ),
            const Spacer(),
            IconButton(
              onPressed: () {},
              icon: SvgPicture.asset(
                ImagesAsset.twoColumns,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
