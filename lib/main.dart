import 'package:anime_player/core/constants/colors.dart';
import 'package:anime_player/core/route/go_router.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const AnimeApp());
}

class AnimeApp extends StatelessWidget {
  const AnimeApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: AppColors.lightTheme ? Brightness.light : Brightness.dark,
      ),
      routerConfig: routes,
    );
  }
}
